class Hira {
  private id: string;
  private num: Number;
  private title: string;
  private version: Number;
  private lyrics: Lyrics[];

  getLyrics() {
    return this.lyrics;
  }
}

enum HiraType {
  'verse',
  'refrain',
  'other'
}

class Lyrics {
  private num: Number;
  private lyrics: string;
  private type: HiraType;
}

interface HiraJson {
  [id: string]: Hira;
}
declare class HrHira extends HTMLElement {
  hira: Object;
  isCurrent: Boolean
}
class HiraManager extends HTMLElement {
  __request_: Request;
  hiraObj: Hira;
  hiras: HiraJson;
  hira: string;
  jsonURL: string;
  private tree: HiraJson;

  constructor(hJson: string) {
    super();
    this.tree = JSON.parse(hJson);
  }

  /**
   * Create hira objects from a sequence of hira given as a string, comma separated.
   * Insensitive to spaces between hira's.  
   * e.g. '512, an12,ff26 , 31' => chain of Promise<Hira> 
   * 
   * @param {string} hirasString 
   * @returns {Promise<Hira>} the chain of promises fulfilled with
   *  object versions of the hira's
   * @memberof HiraManager
   */
  createHiraObjs(hirasString: string) {
    const hirasArr = hirasString.replace(' ', '').split(',');
    hirasArr.map(val => this.getHira(val));
    
    // const hirasAP=new Promise((resolve, reject) => {
    //   const acc = [];
    //   hirasArr.forEach(h => {
    //     this.getHira(h).then(obj => acc.push(obj));
    //   });
    //   resolve(acc);
    // })
    let hirasP = hirasArr.reduce((prev, curr) => {
      return prev.then(_ => this.getHira(curr));
    }, Promise.resolve());
    return hirasP;
  }

  createSections(listOfHira: Hira[]) {
    const parentSection = this.closest('section');
    let section;
    for (let i = 0; i < listOfHira.length; i++) {
      const h = listOfHira[i];
      if (i == 0) {
        section = document.createElement('section');
        const elt = new HrHira();
        elt.hira = h;
        section.insertAdjacentElement('afterbegin', elt);
      }
      else {
        const s = document.createElement('section')
        const elt = new HrHira();
        elt.hira = h;
        s.insertAdjacentElement('afterbegin', elt);
        section.insertAdjacentElement('afterend', s);
      }
    }
    parentSection.insertAdjacentElement('afterend', section);
  }
  
  getHira(which, fromURL = this.jsonURL) {
    /**
     * @type {Hira}
     */
    let res;
    if (this.hiras) {
      this.hiraObj = res = this._getFromObj(this.hiras, which);
      return Promise.resolve(res);
    } else {
      const request = fromURL == this.jsonURL ? this.__request : new Request(fromURL, {
        method: 'GET'
      });
      return fetch(this.__request).then(resp => {
        return resp.json().then(hiras => {
          this.hiras = hiras;
          this.hiraObj = res = this._getFromObj(this.hiras, which);
          return res;
        });
      })
    }
  }

  /**
   * 
   * @param {HiraJson} obj 
   * @param {string} hiraString 
   * @returns 
   * @memberof HiraManager
   */
  _getFromObj(obj, hiraString) {
    const numHira = this._formatHira(hiraString);
    const hira = obj[numHira];
    if (hira.version > 0) {

    }
    hira.lyrics = hira.lyrics.map(val => {
      val.lyrics = val.lyrics.trim();
      return val;
    });
    return hira;
  }

  _formatHira(h = this.hira) {
    h = h.trim();
    const ff = h.match(/ff/i);
    const antema = h.match(/an/i);
    let numHira = h.match(/\d+/)[0].replace(/^0+/, '');

    if (ff) { // fihirana fanampiny
      numHira = 'ff_' + numHira;
    } else {
      if (antema) {
        numHira = 'antema_' + numHira;
      } else {
        numHira = 'ffpm_' + numHira;
      }
    }
    return numHira;
  }
  get __request() {
    return this.__request_ ? this.__request_ : new Request(this.jsonURL, {
      method: 'GET',
      cache: 'default',
      headers: {
        'Cache-Control': 'public, max-age=600'
      }
    });
  }
}

