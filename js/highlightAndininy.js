function toggleSelected() {
   this.classList.toggle('selected');
   //   Polymer.Base.toggleClass('selected', this.hasClass,this);
}

function onParagClick(elt, index, array) {
   if (elt._hasMyClickListeners)
      return;
   else {
      elt._hasMyClickListeners = true;
      elt.addEventListener('click', toggleSelected.bind(elt), false);
   }
}

function onSlideChanged(e) {
   var sectionHira = e.currentSlide || Reveal.getCurrentSlide();
   var parags = sectionHira.querySelectorAll('p.andininy');
   if (parags)
      Array.prototype.forEach.call(parags, onParagClick);
}
Reveal.addEventListener('slidechanged', onSlideChanged);
document.addEventListener('DOMContentLoaded', onSlideChanged, false);