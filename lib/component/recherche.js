Polymer({
         is: 'recherche-hira',

         properties: {
            hiraValue : {
               type : String
               //,observer : '_hiraValueChanged'
            },
            invalid : {
               type : Boolean,
               notify : true,
               value : true
            }
         },
         listeners : {
            'dialog.iron-overlay-opened' : '_onDialogOpen',
            'dialog.iron-overlay-closed' : '_onDialogClosed'
         },

         ready : function() {
               this.listen(this.inputElt,'invalid-changed','_invalidChanged');
            //console.log('this.inputElt= ', this.inputElt);
         },
         
         _invalidChanged : function(e) {
//            if( ! this.hiraValue || this.hiraValue.trim() ==='' ){
//               console.log('invalid');
//               this.invalid=true;
//            }
//            else{
//               this.invalid=this.inputElt.invalid;
//               console.log('valid');
//            }
            this.invalid=e.detail.value;
            console.log(e.detail.value);
            //alert('invalid changed');
         },

         get inputElt() {
            return this.$.inputHira.inputElement;
         },
         changeTheme: function () {
            this.customStyle['--my-toolbar-color'] = 'blue';
            this.updateStyles();
         },

         goToHira: function () {
            'use strict';
            var inputString = this.$.inputHira.value.trim();

            var ff = inputString.match(/ff/i);
            var hiraModulo;
            var nomFichier = '';
            var numFichier = '';
            var numHira = inputString.match(/\d+/);
            numHira=numHira[0].replace(/^0+/,'');

            // nom du fichier
            if (ff) { // fihirana fanampiny
               nomFichier = 'FihiranaFanampiny.html';
               numHira = 'ff_' + numHira;
            } else {
               hiraModulo = Math.floor(parseInt(numHira) / 200) * 200;
               nomFichier = 'fihirana';
               numHira = 'ffpm_' + numHira;

               // numero de fichier
               if (hiraModulo < 200)
                  numFichier = '11-199';
               else if (hiraModulo > 799)
                  numFichier = '800-819';
               else {
                  var dernierNum = hiraModulo + 199;
                  numFichier = hiraModulo.toString() + '-' + dernierNum.toString();
               }
               nomFichier += numFichier + '.html';
            }

            // adresse du hira
            nomFichier += '#/' + numHira;
            var fullUrl = location.origin + location.pathname;
            //fullUrl = fullUrl.slice(0, fullUrl.lastIndexOf('/'));
            fullUrl = fullUrl.slice(0, fullUrl.lastIndexOf('/')) + "/" + nomFichier;
            location.assign(fullUrl);
            this.$.dialog.close();
         },

         hideIfInvalid: function (inputId) {
            var input = document.getElementById(inputId);

            if (input.invalid || !input.value)
               button.setAttribute('hidden', '');
            else {
               button.removeAttribute('hidden');
            }
         },

         clearValue: function (inputId) {
            var input = document.getElementById(inputId);
            input.value = '';
         },

         open : function(){
            this.$.dialog.open();
            this.$.inputHira.value='';
            this.inputElt.focus();
         },
         _onDialogOpen : function(e){
            var alwaysTrue = function(){return false;};
            Reveal.configure({keyboardCondition : alwaysTrue });
         },
         _onDialogClosed : function(e){
            Reveal.configure({keyboardCondition : null });
         }
      });